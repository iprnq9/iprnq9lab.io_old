---
layout: page
title: Projects
categories: projects
permalink: projects/
---

> Brief summaries of some of the things I have worked on.

<ul>
  {% for project in site.projects %}
  <li><a href="{{ project.url }}">{{ project.title }}</a></li>
  {% endfor %}
</ul>