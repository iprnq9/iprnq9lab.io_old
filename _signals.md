---
layout: page
title: Notes
categories: notes
permalink: notes/signals/
---

> Topics involving communications, signal processing, information theory, antennas, radars, systems, and the like.

Euler
<ul>
    {% for note in site.notes %}
    {% if note.categories contains 'euler' %}
    <li><a href="{{ note.url }}">{{ note.title }}</a></li>
    {% endif %}
    {% endfor %}
</ul>

DSP
<ul>
    {% for note in site.notes %}
    {% if note.categories contains 'DSP' %}
    <li><a href="{{ note.url }}">{{ note.title }}</a></li>
    {% endif %}
    {% endfor %}
</ul>

Digital Communications
<ul>
    {% for note in site.notes %}
    {% if note.categories contains 'digital' %}
    <li><a href="{{ note.url }}">{{ note.title }}</a></li>
    {% endif %}
    {% endfor %}
</ul>

Wireless Communications
<ul>
    {% for note in site.notes %}
    {% if note.categories contains 'wireless' %}
    <li><a href="{{ note.url }}">{{ note.title }}</a></li>
    {% endif %}
    {% endfor %}
</ul>

Antennas
<ul>
    {% for note in site.notes %}
    {% if note.categories contains 'antennas' %}
    <li><a href="{{ note.url }}">{{ note.title }}</a></li>
    {% endif %}
    {% endfor %}
</ul>

Radars
<ul>
    {% for note in site.notes %}
    {% if note.categories contains 'radars' %}
    <li><a href="{{ note.url }}">{{ note.title }}</a></li>
    {% endif %}
    {% endfor %}
</ul>
