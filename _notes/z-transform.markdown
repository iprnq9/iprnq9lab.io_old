---
layout: note
title:  "Z-Transform"
categories: [DSP]
---

> The Z-transform is a similar to the Discrete Fourier Transform in that it converts a discrete-time signal into a complex frequency domain representation.

### Definition