---
layout: note
title:  "Chirp Z-Transform"
categories: [DSP]
---

> The Chirp Z-Transform is a generalization of the Discrete Fourier Transform where, rather than traversing the unit circle in the z-domain, a logarithmic spiral can be traversed.

### Definition

The Chirp Z-Transform (CZT) is defined as

$$
\begin{align} \label{eq:dft}
    X(k) &= \sum_{n=0}^{N-1} x(n) \cdot z_k^{-n} \\
    z_k  &= A \cdot W^{-k}
\end{align}
$$

where $$A$$ is the complex starting point, $$W$$ is the complex ratio between points, and $$M$$ is the number of points to calculate.

### CZT in MATLAB
MATLAB has a `czt()` function in the Signal Processing Toolbox. The CZT can be used in MATLAB as shown below. For more information on MATLAB's `czt()` function, refer to [MATLAB documentation](https://www.mathworks.com/help/signal/ref/czt.html).
{% highlight matlab %}
% time domain signal to transform
N       = 1024;             % number of points in x(t)
fs      = 1000;             % sampling frequency; Hz
t       = (0:N-1)./N./fs;   % time vector
x       = gausswin(t);      % time domain signal

% transform parameters
M       = 1024;             % number of points in transform
f1      = 100;              % starting frequency; Hz
f2      = 150;              % stopping frequency; Hz

% complex starting point
A       = exp(1j*2*pi*f1/fs);

% complex step size
W       = exp(-1j*2*pi*(f2-f1)/(M*fs));

% take CZT; in order to use the forward transform, must use conjugates
x       = czt(x,M,W,A)' ./ M;

% create time vector
t = t1 + (0:M-1).*(t2-t1) ./ (M-1);
{% endhighlight %}

### Frequency to Time
The CZT can be used to convert from frequency domain data to time domain data in MATLAB as shown below. However, the bandwidth of band-limited frequency data determines the actual resolution in the time domain by the following relationship.

$$T_s = 1 / BW$$

The maximum amount of time domain data is then

$$ T_f = T_s \times M $$

since there are $$M$$ points with step size of $$T_s$$ between points.

{% highlight matlab %}
% transform parameters
M       = 1024;     % number of points in transform
BW      = f2 - f1;  % bandwidth of X(f); Hz
T       = 1 / BW;   % time resolution; seconds
Tf      = T * N;    % total time; seconds

t1      = 0;        % starting time; seconds
t2      = 64 * T;   % stopping time; seconds

% complex starting point
A       = exp(1j*2*pi*t1/Tf);

% complex step size
W       = exp(-1j*2*pi*(t2-t1)/((M-1)*Tf));

% take CZT; in order to use the forward transform, must use conjugates
x       = czt(X',M,W,A)' ./ M;

% create time vector
t = t1 + (0:M-1).*(t2-t1)./(M-1);
{% endhighlight %}

Suggested reading material:
* Link to article
* Book and author
* IEEE paper