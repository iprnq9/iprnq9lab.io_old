---
layout: note
title:  "FIR Filter Design By Window Method"
categories: [DSP]
---

> If asked to design an FIR filter with a desired cutoff frequency, how would we find an appropriate filter impulse response?

### Ideal Filters

Let's assume we want to create an ideal low-pass filter (LPF) with a cutoff frequency $$\omega_c$$.

Being ideal, the filter should completely pass all frequencies below $$\omega_c$$ and reject all frequencies above $$\omega_c$$. This is known as a "brick-wall" filter as exemplified by the figure below.

![Ideal LPF frequency response](/images/dsp/fir-filter-design-window-method/lpf-brickwall.svg#center, "Ideal LPF frequency response")

As you may recall, the sharp transitions associated with the frequency response $$H(f)$$ of such a filter yields an impulse response $$h(n)$$ exhibiting some flavor of the $$sinc$$ function. The $$sinc$$ function only decays completely to zero at its ends at positive and negative infinity. Consequently, the impulse response of a brick-wall filter cannot be completely realized since we can only have a finite number of taps in an actual system. Truncating the impulse response of the filter can produce satisfactory results. The comparison below exemplifies this truncation.


#### An "infinitely" long impulse response

Using MATLAB, I created a *really* long filter (100,000 taps) which may not be infinitely long, but it gets the point across. Its impulse and frequency magnitude responses are shown below. As you can see, it has quite a sharp transition from passband to stopband which resembles the ideal brick-wall filter.

![A really long impulse response](/images/dsp/fir-filter-design-window-method/really_long_sinc.svg#center, "A really long impulse response")


#### The effect of truncation

If we truncate that sinc to be only 100 taps long, we can see that its frequency magnitude response is not nearly as ideal as we would have liked. The transition from passband to stopband is not as sharp and side-lobes have crept up higher.

![Truncating the impulse response](/images/dsp/fir-filter-design-window-method/truncated_sinc.svg#center, "Truncating the impulse response")


### Using Window Functions to Create Realistic Filters

We can create a more desirable filter by multiplying the truncated impulse response by a window function of choice. [Wikipedia has a great page on Window Functions](https://en.wikipedia.org/wiki/Window_function). Recall that there are several popular windowing functions:
- Hamming
- Hann
- Blackman
- Blackman-Harris
- Gaussian
- Kaiser
- ...

The window functions are especially useful because they allow us to suppress the side-lobes at a cost of widening the main lobe.

To use window functions in FIR filter design, we multiply the truncated impulse response of the ideal filter by the window function of choice, as described by the equation below.

 $$h(n) = h_{i}(n) \ w(n)$$

The effect of windowing on the impulse and frequency magnitude responses can be seen in the figure below (Hamming window was used).

![The effect of windowing](/images/dsp/fir-filter-design-window-method/really_long_sinc.svg#center, "The effect of windowing")


### MATLAB Example

{% highlight matlab %}
% Code here
{% endhighlight %}


### Summary

To create FIR filters using the window method:
1. Determine the ideal impulse response (based on cutoff frequency)
2. Truncate the impulse response to the centermost $$M+1$$ samples (M is the order of the filter)
3. Shift the impulse response by $(M+1)/2$ samples so that the filter is causal
4. Multiply by the window function