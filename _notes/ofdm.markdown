---
layout: note
title:  "Orthogonal Frequency-Division Multiplexing (OFDM)"
categories: [wireless]
---

> OFDM is a method for spreading a signal out over multiple frequencies, yielding many benefits in wireless communication.

### Definition