---
layout: note
title:  "Discrete Fourier Transform"
categories: [signals, DSP]
---

> The Discrete Fourier Transform is used for Fourier analysis by transforming a discrete-time signal into complex discrete-frequency components.

$$
\begin{align}\label{eq:dft}
    X(k) &= \sum_{n=0}^{N-1} x[n] e^{-j 2\pi \frac{kn}{N}} \\
         &= \sum_{n=0}^{N-1} x(n) \left( \cos(2\pi \frac{kn}{N}) - j \sin(2\pi \frac{kn}{N}) \right)
\end{align}
$$

The DFT is typically implemented using the Fast Fourier Transform (FFT) algorithm by Cooley and Tukey. Please refer to this page for more information on the FFT.

Use of the FFT in MATLAB can be seen below.
{% highlight matlab %}
% Number of points in FFT result
nfft = 1024;

% FFT
X = fft(x,nfft);

% Create frequency vector
f = (0:nfft-1).*fs/nfft;

% Plot magnitude
figure();
plot(f,abs(X));
grid on;
xlabel('Frequency (Hz)');
ylabel('Magnitude (linear)');
{% endhighlight %}

Suggested reading material:
* Link to article
* Book and author
* IEEE paper