---
layout: project
title:  "Digital Self-Interference Cancellation for In-Band Full Duplex"
---

> In recent years, there has been significant progress in realizing in-band full-duplex, the capability for a radio to simultaneously transmit and receive on the same channel (e.g., time and frequency). What has traditionally prevented in-band full-duplex is the undesired self-interference between a radio’s transmitter and its receiver which corrupts any desired receive signal. In order to reliably receive a desired signal, this self-interference must be removed from the total received signal by what is known as self-interference cancellation which typically occurs in two stages: analog cancellation and digital cancellation.

Perhaps the most attractive quality of a full-duplex (FD) radio is the increased spectral efficiency which can be up to twice that of an equivalent half-duplex radio since transmission and reception occur over the same time-frequency resource. With the growing number of wireless devices, spectrum availability is becoming more scarce increasing its demand, while the demand for higher data rates has increased. FD has shown promise in satisfying these challenges by eliminating the need for frequency-division duplexing (FDD) while providing simultaneous transmission and reception. In addition to commercial wireless communications, the application of FD extends to military communication and radar as well as consumer cable television and internet. Along with increased spectrum efficiency, FD offers MAC and network gains.

What has long made FD difficult to achieve is the undesired coupling between a radio's TX and its RX in what is referred to as self-interference (SI). Since a desired receive signal is likely to be very weak due to the loss incurred in its travel, the SI power is often many orders of magnitude higher than that of the desired receive signal, or signal of interest (SoI). Therefore, in order to reliably detect and demodulate the SoI, the SI must be removed from the total received signal by what is known as self-interference cancellation (SIC). In fact, to offer the same signal-to-interference-plus-noise ratio as an equivalent half-duplex system to an incoming SoI, a FD radio would need to cancel the SI completely to the thermal noise floor of its RX which may require 100 dB or more of total SIC. In order to achieve the necessary SIC, FD systems typically employ two stages of cancellation: analog SIC (ASIC) and digital SIC (DSIC).

![Linear and nonlinear DSIC](/images/dsic/linear-nonlinear-dsic.svg#center, "Linear and nonlinear DSIC")

The figure above shows the results of two forms of DSIC. Observe the strength of the SI which would corrupt a desired receive signal. Linear DSIC can be seen to cancel 31.7 dB of the SI. Nonlinear DSIC is even more effective, achieving 37.3 dB of SIC. The difference between linear and nonlinear cancellation is the complexity of the basis function used in estimation. Linear DSIC assumes only linear components, while nonlinear DSIC considers nonlinearities in the SI signal.

### References:
 1. [D. Bharadia et al., *Full Duplex Radios*](https://web.stanford.edu/~skatti/pubs/sigcomm13-fullduplex.pdf)
 2. [D. Korpi, *Thesis: Full-Duplex Wireless: Self-Interference Modeling, Digital Cancellation, and System Studies*](https://theses.eurasip.org/media/theses/documents/korpi-dani-full-duplex-wireless-self-interference-modeling-digital-cancellation-and-system-studies.pdf)
 3. [D. Korpi et al., *Widely-Linear Digital Self-Interference Cancellation in Direct-Conversion Full-Duplex Transceiver*](https://arxiv.org/pdf/1402.6083.pdf)
