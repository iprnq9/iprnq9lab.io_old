---
layout: default
---

I am a graduate student the [electrical engineering program](https://www.ece.utexas.edu) at [The University of Texas at Austin](https://utexas.edu). My academic focus is on wireless commuinication, signal processing, and full-duplex communication. I am supervised by [Prof. Sriram Vishwanath](http://users.ece.utexas.edu/~sriram/) and have been part of the [Laboratory of Informatics, Networks and Communications (LINC)](http://sriram.utlinc.org/#/utlinc) within the [Wireless Netowrking and Communications Group (WNCG)](https://wncg.org).

I received my Bachelor of Science in Electrical Engineering from [Missouri University of Science and Technology](https://mst.edu) in May 2018. During which, I was involved in undergraduate research supervised by [Prof. Y. Rosa Zheng](https://www.lehigh.edu/~yrz218/) on the topic of underwater acoustic commuinication.


## Interests

My interests include wireless commuinication, signal processing, and full-duplex commuinication. Specifically, my interests and skills include PHY layer, MIMO, mmWave, 4G LTE and 5G NR cellular systems, spectrum sharing, adaptive modulation, and communication system design and simulation.


## Projects

 <ul>
    {% for project in site.projects %}
    <li><a href="{{ project.url }}">{{ project.title }}</a></li>
    {% endfor %}
 </ul>

## Education

**The University of Texas at Austin**, _August 2018 -- Present_  
M.S. Electrical Engineering  

**Missouri University of Science and Technology**, _August 2014 -- May 2018_  
B.S. Electrical Engineering


## Experience

**The University of Texas at Austin**, _August 2018 -- Present_  
Graduate Research Assistant, Wireless Networking and Communications Group (WNCG)

**GenXComm, Inc**, _May 2018 -- Present_  
Wireless Engineering Intern

**Sandia National Laboratories**, _May 2017 -- May 2018_  
R&D Electrical Engineering Intern

**Missouri University of Science and Technology**, _January 2017 -- May 2018_  
Undergraduate Research Assistant

**Dynetics, Inc**, _Summer 2016_  
Electrical Engineering Intern


## Graduate Coursework

 - Wireless Communication (Fall 2018, Andrews)
 - Probability and Stochastic Processes I (Fall 2018, Shakkottai)
 - Space-Time Communication (Spring 2019, Heath)
 - Real-Time Digital Signal Processing (Spring 2019, Evans)
 - Coding Theory (Spring 2019, Zuckerman)

 
## Contact
Email: ipr [at] utexas [dot] edu
