---
layout: default
---

Hello, my name is Ian. In the fall, I will be a graduate student the electrical engineering program at The University of Texas at Austin. My undergrad was at Missouri University of Science and Technology in Rolla, Missouri. I enjoy learning new things, riding my bike, playing frisbee, and playing around on the computer.

This site is meant to provide you and I some reference material in the areas of wireless communications, digital signal processing, information theory, and the like. I hope you find it useful and a worthy supplement to your courses, reading, and/or scouring of the web. While I hope it is in fact all correct, I make no guarantee or warranty regarding the accuracy of the content on this site.

